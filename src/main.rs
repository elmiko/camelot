/*
    Copyright (C) 2021 camelot authors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::net::{
    Ipv4Addr,
    SocketAddrV4,
    SocketAddr
};
use warp::Filter;

mod docstores;
mod filters;
mod handlers;
mod responses;

pub const VERSION: &str = "0.1.0";

#[tokio::main]
async fn main() {
    println!("Starting Camelot server version {}", VERSION);

    let storage = docstores::blank_memory_store();

    let addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 8080));

    let server_info = warp::get()
            .and(warp::path::end())
            .map(|| Ok(responses::server_info()));

    let docs = filters::docs(storage);

    let routes = server_info
        .or(docs)
        .recover(handlers::errors);

    println!("Listening on http://{}", addr);
    warp::serve(routes)
        .run(addr)
        .await;
}
