/*
    Copyright (C) 2021 camelot authors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::{
    VERSION,
};
use warp::http::{
    Response,
    StatusCode,
};
use yaml_rust::{
    yaml,
    Yaml,
    YamlEmitter,
};

fn build_response(code: StatusCode, body: yaml::Hash) -> warp::http::Response<String> {
    let mut buf = String::new();
    let mut emitter = YamlEmitter::new(&mut buf);
    let body = Yaml::Hash(body);
    emitter.dump(&body).unwrap();
    buf.push('\n');

    Response::builder()
        .status(code)
        .header("content-type", "application/yaml")
        .body(buf).unwrap()
}

pub fn doc_update(uuid: String, create: bool) -> Result<warp::http::Response<String>, warp::http::Error> {
    let mut body = yaml::Hash::new();
    let mut data = yaml::Hash::new();
    let location = String::from(format!("/doc/{}", uuid.clone()));
    data.insert(Yaml::String("id".to_string()), Yaml::String(uuid));
    data.insert(Yaml::String("location".to_string()), Yaml::String(location));
    body.insert(Yaml::String("type".to_string()), Yaml::String("document".to_string()));
    body.insert(Yaml::String("data".to_string()), Yaml::Hash(data));

    let code = match create {
        true => StatusCode::CREATED,
        false => StatusCode::OK,
    };
    Ok(build_response(code, body))
}

pub fn error(code: StatusCode, message: String) -> Result<warp::http::Response<String>, warp::http::Error> {
    let mut body = yaml::Hash::new();
    let mut data = yaml::Hash::new();
    data.insert(Yaml::String("status".to_string()), Yaml::String(code.to_string()));
    data.insert(Yaml::String("message".to_string()), Yaml::String(message));
    body.insert(Yaml::String("type".to_string()), Yaml::String("error".to_string()));
    body.insert(Yaml::String("data".to_string()), Yaml::Hash(data));

    Ok(build_response(code, body))
}

pub fn server_info() -> Result<warp::http::Response<String>, warp::http::Error> {
    let mut body = yaml::Hash::new();
    let mut data = yaml::Hash::new();
    data.insert(Yaml::String("version".to_string()), Yaml::String(VERSION.to_string()));
    body.insert(Yaml::String("type".to_string()), Yaml::String("server".to_string()));
    body.insert(Yaml::String("data".to_string()), Yaml::Hash(data));

    Ok(build_response(StatusCode::OK, body))
}
