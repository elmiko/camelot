/*
    Copyright (C) 2021 camelot authors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::{
    docstores,
    handlers,
};
use warp::{
    Filter,
    Rejection,
};
use warp::reply::Reply;

pub fn doc_create(storage: docstores::MemoryStore) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::put()
        .and(warp::path("doc"))
        .and(warp::header::exact("content-type", "application/yaml"))
        .and(warp::body::bytes())
        .and(warp::any().map(move || storage.clone()))
        .and_then(handlers::doc_create)
}

pub fn doc_read(storage: docstores::MemoryStore) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::get()
        .and(warp::path("doc"))
        .and(warp::path::param())
        .and(warp::any().map(move || storage.clone()))
        .and_then(handlers::doc_read)
}

pub fn doc_update(storage: docstores::MemoryStore) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::put()
        .and(warp::path("doc"))
        .and(warp::path::param())
        .and(warp::header::exact("content-type", "application/yaml"))
        .and(warp::body::bytes())
        .and(warp::any().map(move || storage.clone()))
        .and_then(handlers::doc_update)
}

pub fn docs(storage: docstores::MemoryStore) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    doc_read(storage.clone())
        .or(doc_update(storage.clone()))
        .or(doc_create(storage.clone()))
}
