/*
    Copyright (C) 2021 camelot authors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::{
    docstores,
    responses,
};
use std::convert::Infallible;
use uuid::Uuid;
use warp::Rejection;
use warp::http::{
    Response,
    StatusCode,
};
use warp::reply::Reply;
use yaml_rust::{
    YamlLoader,
    YamlEmitter,
};

pub async fn doc_create(body: warp::hyper::body::Bytes, storage: docstores::MemoryStore) -> Result<impl Reply, Infallible> {
    let mut s = storage.lock().await;
    let body = String::from_utf8(body.to_vec()).unwrap();
    if &body[0..4] != "---\n" {
        let message = String::from("Body not understood as yaml");
        return Ok(responses::error(StatusCode::UNSUPPORTED_MEDIA_TYPE, message))
    }
    match YamlLoader::load_from_str(&body.as_str()) {
        Ok(docs) => {
            let uuid = Uuid::new_v4().to_hyphenated().to_string();
            let doc = &docs[0];
            let mut outbuffer = String::new();
            let mut emitter = YamlEmitter::new(&mut outbuffer);
            emitter.dump(doc).unwrap();
            s.insert(uuid.clone(), outbuffer.clone());
            Ok(responses::doc_update(uuid, true))
        },
        Err(_) => {
            let message = String::from("Body not understood as yaml");
            Ok(responses::error(StatusCode::UNSUPPORTED_MEDIA_TYPE, message))
        }
    }
}

pub async fn doc_read(uuid: String, storage: docstores::MemoryStore) -> Result<impl Reply, Infallible> {
    let s = storage.lock().await;
    let code;
    let mut message;
    match s.get(&uuid) {
        Some(doc) => {
            code = StatusCode::OK;
            message = String::from(doc);
            message.push('\n');
            Ok(Response::builder()
               .status(code)
               .header("content-type", "application/yaml")
               .body(message))
        },
        _ => {
            let message = String::from("Not found");
            Ok(responses::error(StatusCode::NOT_FOUND, message))
        }
    }
}

pub async fn doc_update(uuid: String, body: warp::hyper::body::Bytes, storage: docstores::MemoryStore) -> Result<impl Reply, Infallible> {
    let mut s = storage.lock().await;
    if !s.contains_key(&uuid) {
        let message = String::from("Not found");
        return Ok(responses::error(StatusCode::NOT_FOUND, message))
    }

    let body = String::from_utf8(body.to_vec()).unwrap();
    if &body[0..4] != "---\n" {
        let message = String::from("Body not understood as yaml");
        return Ok(responses::error(StatusCode::UNSUPPORTED_MEDIA_TYPE, message))
    }
    match YamlLoader::load_from_str(&body.as_str()) {
        Ok(docs) => {
            let doc = &docs[0];
            let mut outbuffer = String::new();
            let mut emitter = YamlEmitter::new(&mut outbuffer);
            emitter.dump(doc).unwrap();
            s.insert(uuid.clone(), outbuffer.clone());
            Ok(responses::doc_update(uuid, false))
        },
        Err(_) => {
            let message = String::from("Body not understood as yaml");
            Ok(responses::error(StatusCode::UNSUPPORTED_MEDIA_TYPE, message))
        }
    }
}

pub async fn errors(err: Rejection) -> Result<impl Reply, Rejection> {
    let code;
    let message;

    if err.is_not_found() {
        code = StatusCode::NOT_FOUND;
        message = String::from("Not found");
    } else if let Some(_) = err.find::<warp::reject::MethodNotAllowed>() {
        code = StatusCode::METHOD_NOT_ALLOWED;
        message = String::from("Method not allowed");
    } else if let Some(e) = err.find::<warp::reject::InvalidHeader>() {
        match e.name() {
            "accept" => {
                code = StatusCode::NOT_ACCEPTABLE;
                message = String::from("Not acceptable");
            },
            _ => {
                code = StatusCode::BAD_REQUEST;
                message = format!("Invalid header: {}", e.name());
            }
        }
    } else {
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = String::from("Internal error");
    }

    Ok(responses::error(code, message))
}
