# Contributing

Welcome, camelot is an open source project that encourages collaboration from the public.
If you are interested in getting involved there are a couple things to know first.

## Bugs and Feature Requests

If you have encountered a bug, or would like to see a feature added to camelot, please
open a [new issue](https://gitlab.com/elmiko/camelot/-/issues) describing what you have
found or would like to request.

For bug reports please leave instructions on how to recreate the error you encountered.

## Merge Requests

If you have a change to propose, please fork the repository, create a branch for your
change and open a [merge request](https://gitlab.com/elmiko/camelot/-/merge_requests)
againt the appropriate branch.

Don't forget to update the [Changelog](/CHANGELOG.md) and add your name to the [Authors](/AUTHORS.md).

### Branch Layout

The default branch for camelot is named `devel`, all primary work happens from this branch.
This is where all new features and bug fixes should land.

Release branches are created for each major and minor release, following a loose [semver](https://semver.org/)
style versioning. Patch level fixes should be proposed against the appropriate release and then
cherry picked back into the devel branch.

For example, when releasing version `1.2.0`, a new branch will be created named `release-1.2`.
Any patch level fixes, eg `1.2.1`, would be applied to the release branch.

Release tags are created for every release (major, minor, and patch), and point to the appropriate
branch where necessary.
