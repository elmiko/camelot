FROM registry.access.redhat.com/ubi8/ubi:latest as builder

RUN dnf install -y gcc
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --quiet

COPY Cargo.* /opt/app-root/
COPY src /opt/app-root/src/

WORKDIR /opt/app-root
RUN source $HOME/.cargo/env && \
    cargo build --release


FROM registry.access.redhat.com/ubi8/ubi-minimal:latest

WORKDIR /opt
COPY --from=builder /opt/app-root/target/release/camelot .
ENTRYPOINT ["/opt/camelot"]
