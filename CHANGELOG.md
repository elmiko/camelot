# Changelog

## devel

* add dockerfile
* change default address to `0.0.0.0:8080`

## 0.1.0

* initial release
