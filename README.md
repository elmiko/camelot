# Camelot

a very silly document server

## here there be dragons

* Camelot only speaks the made up mime type `application/yaml`
* all requests must set `Accept` to something that will match `application/yaml`
* sending data must set `Content-Type` to `application/yaml`

## quickstart

**start the local server**
```bash
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.06s
     Running `target/debug/camelot`
Starting Camelot server version 0.0.1
Listening on http://127.0.0.1:8080
```

**get server info**
```bash
$ curl http://127.0.0.1:8080/
---
type: server
data:
  version: 0.0.1
```

**create a document**
```bash
$ cat hello.yaml
---
message: hello

$ curl -H "content-type: application/yaml" \
       -X PUT \
       --data-binary @hello.yaml \
       http://127.0.0.1:8080/doc
---
type: document
data:
  id: 2d63dcfa-6a97-48b1-be13-a8c016d12c74
  location: /doc/2d63dcfa-6a97-48b1-be13-a8c016d12c74
```

**read a document**
```bash
$ curl http://127.0.0.1:8080/doc/2d63dcfa-6a97-48b1-be13-a8c016d12c74
---
message: hello
```

**update a document**
```bash
$ cat helloworld.yaml
---
message: hello world!

$ curl -H "content-type: application/yaml" \
       -X PUT \
       --data-binary @helloworld.yaml \
       http://127.0.0.1:8080/doc/2d63dcfa-6a97-48b1-be13-a8c016d12c74
---
type: document
data:
  id: 2d63dcfa-6a97-48b1-be13-a8c016d12c74
  location: /doc/2d63dcfa-6a97-48b1-be13-a8c016d12c74
$ curl http://127.0.0.1:8080/doc/2d63dcfa-6a97-48b1-be13-a8c016d12c74
---
message: hello world!
```
